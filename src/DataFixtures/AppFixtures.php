<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Contact;
use App\Entity\Itinerary;
use Faker\Factory as Faker;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker::create('fr_FR');

        // Creation de 10 trajets
        $itinerarys = [];
        for ($i = 0; $i < 15; ++$i) {
            $itinerary = new Itinerary();
            $itinerary->setResource('OSRM');
            $itinerary->setStartPointLatitude($faker->latitude);
            $itinerary->setStartPointLongitude($faker->longitude);
            $itinerary->setEndPointLatitude($faker->latitude);
            $itinerary->setEndPointLongitude($faker->longitude);
            $itinerary->setDeparture($faker->dateTime);
            $itinerary->setArrival($faker->dateTime);
            $itinerary->setDepartureHour($faker->dateTime);
            // On utilise ensuite le manager pour persister
            $manager->persist($itinerary);
            // On l'ajoute dans le tableau des trajets
            $itinerarys[] = $itinerary;
        }

        //contact
        $contact=[];
        for ($i=0; $i < 5 ; $i++) { 
            $contact = new Contact();
            $contact->setFirstName($faker->firstName);
            $contact->setLastName($faker->lastName);
            $contact->setEmail($faker->email);
            $contact->setPhone($faker->phoneNumber);
            $contact->setSujet($faker->text);
            $contact->setMessage($faker->text);

            // On utilise ensuite le manager pour persister
            $manager->persist($contact);
            // On l'ajoute dans le tableau des trajets
            $contacts[] = $contact;
        }


        $manager->flush();
    }
}
