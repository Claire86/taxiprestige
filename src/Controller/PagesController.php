<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Contact;
use App\Form\BookingType;
use App\Form\ContactType;
use App\Service\MailerService;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class PagesController extends AbstractController
{

    public function __construct(
        private readonly MailerService $mailerService)
    {}

    #[Route('/', name: 'home')]
    public function home(): Response
    {
        $apiKey = 'Z29vZ2xlLW9hdXRoMnwxMDMyMDg0NTg5NjU1ODA4MTE3MTB8OTI5NDZkNTExNg'; // Remplacez par la clé API réelle
        return $this->render('pages/index.html.twig', ['apiKey' => $apiKey]);
    }

    #[Route('/services', name: 'offers')]
    public function service(): Response
    {
        return $this->render('pages/offer.html.twig');
    }

    #[Route('/tarifs', name: 'price')]
    public function price(): Response
    {
        return $this->render('pages/price.html.twig');
    }

    #[Route('/reserver', name: 'booking')]
    public function booking(
        Request $request, 
        EntityManagerInterface $entityManager
    ): Response
    {
        // Pour verifier que le service fonctionne bien.

        $booking = new Booking();

        //on crée le formulaire (le nom du formulaire, les données)
        $bookingForm = $this->createForm(BookingType::class, $booking);

        $bookingForm->handleRequest($request);
         // Initialiser les variables d'alerte par défaut
         $alertClass = '';
         $alertMessage = '';

        if ($bookingForm->isSubmitted() && $bookingForm->isValid()) {
            $booking = $bookingForm->getData();

            if (!$bookingForm->get('acceptTerms')->getData()) {
                $bookingForm->get('acceptTerms')->addError(new FormError('Vous devez accepter les termes et conditions.'));
            } else {
                // Persist the booking entity
                $entityManager->persist($booking);
                // Flush changes to the database
                $entityManager->flush();
                // Créer un nouveau formulaire vide après soumission et validation
                $bookingForm = $this->createForm(BookingType::class, new Booking());

                try {
                    $this->mailerService->sendEmail(
                        replyTo: $booking->getEmail(),
                        subject: 'Demande de réservation',
                        template: 'booking',
                        context: $booking->toArray()
                    );
                    // Ajouter une classe CSS pour l'alerte de succès
                    $alertClass = 'alert-success';
                    $alertMessage = 'Merci pour votre demande de réservation. Celle-ci a bien été envoyé. Nous nous engageons a vous répondre sous 12h maximum. Vous recevrez un mail de confirmation si cette demande est validée.';
                } catch (\Exception $e) {
                    // Ajouter une classe CSS pour l'alerte d'erreur
                    $alertClass = 'alert-danger';
                    $alertMessage = 'Une erreur s\'est produite lors de l\'envoi de l\'e-mail';
                }
            }
        }
        return $this->render('pages/booking.html.twig', [
            'bookingForm' => $bookingForm->createView(),
            'alertClass' => $alertClass,
            'alertMessage' => $alertMessage,
            // 'data' => $adresseService->getAddressSuggestions(),
        ]);
    }

    #[Route('/contact', name: 'contact')]
    public function contact(Request $request): Response
    {
        // On crée un nouveau 'contact'
        $contact = new Contact();
    
        // On crée le formulaire (le nom du formulaire, les données)
        $contactForm = $this->createForm(ContactType::class, $contact);
    
        $contactForm->handleRequest($request);
    
        // Initialiser les variables d'alerte par défaut
        $alertClass = '';
        $alertMessage = '';
    
        if ($contactForm->isSubmitted() && $contactForm->isValid()) {
            // On récupère les données du formulaire
            $contact = $contactForm->getData();
    
            if (!$contactForm->get('acceptTerms')->getData()) {
                // Si la case à cocher n'est pas cochée, on ajoute une erreur au champ
                $contactForm->get('acceptTerms')->addError(new FormError('Vous devez accepter les termes et conditions.'));
            } else {
                // Si la case à cocher est cochée, on peut continuer avec le traitement
                try {
                    $this->mailerService->sendEmail(
                        replyTo: $contact->getEmail(),
                        subject: $contact->getSujet(),
                        template: 'contact',
                        context: $contact->toArray()
                    );
                    // Ajouter une classe CSS pour l'alerte de succès
                    $alertClass = 'alert-success';
                    $alertMessage = 'Votre demande a bien été envoyé.Nous traiterons votre demande dans les plus brefs délais.';
                } catch (\Exception $e) {
                    // Ajouter une classe CSS pour l'alerte d'erreur
                    $alertClass = 'alert-danger';
                    $alertMessage = 'Une erreur s\'est produite lors de l\'envoi de l\'e-mail';
                }
            }
        }
    
        // Renvoyer la réponse même si le formulaire n'a pas été soumis ou n'est pas valide
        return $this->render('pages/contact.html.twig', [
            'contactForm' => $contactForm->createView(),
            'alertClass' => $alertClass,
            'alertMessage' => $alertMessage,
        ]);
    }
    #[Route('/mention', name: 'mention')]
    public function mentions(): Response
    {
        return $this->render('pages/mentions.html.twig');
    }
    
}
