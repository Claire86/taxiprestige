<?php

namespace App\Controller\Admin;

use App\Entity\Price;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PriceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Price::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TimeField::new('startHour')
                ->setFormat('HH:mm')
                ->setLabel('Heure de début'),
            TimeField::new('endHour')
                ->setFormat('HH:mm')
                ->setLabel('Heure de fin'),
            TextField::new('startDay')
                ->setLabel('Jour de début'),
            TextField::new('endDay')
                ->setLabel('Jour de fin'),
            TextField::new('day')
                ->setLabel('Jour'),
            NumberField::new('amount')
                ->setLabel('Tarif'),
        ];
    }
}
