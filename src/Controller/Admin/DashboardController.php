<?php

namespace App\Controller\Admin;

use App\Entity\Price;
use App\Entity\Option;
use App\Entity\Booking;
use App\Entity\Contact;
use App\Entity\Itinerary;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\Admin\BookingCrudController;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(BookingCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Taxiprestige');
    }

    public function configureMenuItems(): iterable
    {
        // yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Reservation', 'fas fa-rectangle-list', Booking::class);
        yield MenuItem::linkToCrud('Les tarifs', 'fas fa-euro-sign', Price::class);
        yield MenuItem::linkToCrud('Les trajets', 'fas fa-location-dot', Itinerary::class);
        yield MenuItem::linkToCrud('Les options', 'fas fa-plus', Option::class);
        yield MenuItem::linkToCrud('Demande de contact', 'fas fa-envelope', Contact::class);
    }
}
