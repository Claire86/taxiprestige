<?php

namespace App\Controller\Admin;

use App\Entity\Booking;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BookingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Booking::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            // IdField::new('id'),
            TextareaField::new('message'),
            DateField::new('created_at')->setFormTypeOption('input', 'datetime_immutable'),
            ChoiceField::new('isValidated')
            ->setChoices([
                'Non' => 0,
                'Oui' => 1,
            ])
            ->allowMultipleChoices(false) // Pour s'assurer qu'un seul bouton peut être sélectionné
            ->renderExpanded(true), // Pour afficher les boutons radio sur une seule ligne (non en liste déroulante)
            // AssociationField::new('itinerary'),
        ];
    }
}
