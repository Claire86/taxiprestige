<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName',options:[
                'label'=>'Prenom',
                'attr'=>[
                    'placeholder'=> "Entrer votre prénom"
                ],
            ])
            ->add('lastName', options:[
                'label'=>'Nom*',
                'attr'=>[
                    'placeholder'=> "Entrer votre nom de famille"
                ],
            ])
            ->add('email', options:[
                'label'=>'Email*',
                'attr'=>[
                    'placeholder'=> "Entrer votre adresse email"
                ],
            ])
            ->add('phone', options:[
                'label'=>'Téléphone',
                'attr'=>[
                    'placeholder'=> "Entrer votre numéro de téléphone"
                ],
            ]) 
            ->add('sujet', options:[
                'label'=>'Sujet*',
                'attr'=>[
                    'placeholder'=> "Le sujet de votre message"
                ],
            ])
            ->add('message', options:[
                'label'=>'Message*',
                'attr'=>[
                    'rows'=>5,
                    'placeholder'=> "Ecrivez votre message"
                ],
            ])
            ->add('acceptTerms', CheckboxType::class, [
                'label' => 'En validant ce formulaire, j\'accepte que mes données soient utilisées dans le cadre de la demande de contact.*',
                'mapped' => false, // Ne pas mapper cette propriété à un champ de l'entité
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les termes et conditions.',
                    ]),
                ],
            ])
           
            ->add('submit',SubmitType::class,[
                'label'=> 'Envoyer'
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
