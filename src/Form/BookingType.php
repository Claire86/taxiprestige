<?php

namespace App\Form;

use App\Entity\Booking;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder        
            ->add('firstName', options:[
                'label'=>'Prénom',
                'attr'=>[
                    'placeholder'=> "Entrer votre prénom"
                ],
            ]) 
            ->add('lastName', options:[
                'label'=>'Nom de famille*',
                'attr'=>[
                    'placeholder'=> "Entrer votre nom de famille"
                ],
            ]) 
            ->add('email', options:[
                'label'=>'Email*',
                'attr'=>[
                    'placeholder'=> "Entrer votre adresse mail"
                ],
            ]) 
            ->add('phone', options:[
                'label'=>'Téléphone*',
                'attr'=>[
                    'placeholder'=> "N°de téléphone"
                ],
            ]) 
            ->add('day', DateTimeType::class,[
                'widget' => 'single_text',
                'label'=>'Date et heure de réservation*',
            ]) 
           
            ->add('departure', options:[
                'label'=>'Adresse de départ*',
                'attr'=>[
                    'placeholder'=> "Entrer votre lieu de départ"
                ],
            ]) 
            ->add('arrival', options:[
                'label'=>'Adresse de destination*',
                'attr'=>[
                    'placeholder'=> "Entrer votre lieu d'arrivée"
                ],
            ]) 
            ->add('numberUser', options:[
                'label'=>'Nombre de passagers *',
            ]) 
            ->add('suitcase', options:[
                'label'=>'Nombre de bagages',
            ])  
            ->add('message', options:[
                'label'=>'Message',
                'attr'=>[
                    'placeholder'=> "Autre chose?"
                ],
            ])
            ->add('acceptTerms', CheckboxType::class, [
                'label' => 'En validant ce formulaire, j\'accepte que mes données soient utilisées dans le cadre de la demande de contact.*',
                'mapped' => false, // Ne pas mapper cette propriété à un champ de l'entité
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les termes et conditions.',
                    ]),
                ],
            ])
            ->add('submit',SubmitType::class,[
                'label'=> 'Envoyer'
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }
}
