<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailerService {

    public function __construct(private MailerInterface $mailer, private $fromEmail, private $toEmail){
        $this->fromEmail = $fromEmail;
        $this->toEmail = $toEmail;
    }

    public function sendEmail(
        string $replyTo = 'you@example.com',
        string $subject = 'Time for Symfony Mailer!',
        array $context = [],
        string $template = 'contact',
        ?string $to = null,
        ) : void
    {
        $email = (new TemplatedEmail())
            ->from($this->fromEmail)
            ->to($to ?? $this->toEmail)
            ->replyTo($replyTo)
            ->subject($subject)
            ->htmlTemplate("emails/$template.html.twig")
            ->context($context);

        $this->mailer->send($email);

    }
}