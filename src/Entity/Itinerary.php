<?php

namespace App\Entity;

use App\Repository\ItineraryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ItineraryRepository::class)]
class Itinerary
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $resource = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 11, scale: 8)]
    private ?string $startPointLatitude = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 11, scale: 8)]
    private ?string $startPointLongitude = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 11, scale: 8)]
    private ?string $endPointLatitude = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 11, scale: 8)]
    private ?string $endPointLongitude = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2, nullable: true)]
    private ?string $duration = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2, nullable: true)]
    private ?string $distance = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $departure = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $arrival = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $departure_hour = null;

    #[ORM\ManyToOne(inversedBy: 'itinerarys')]
    private ?Price $price = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResource(): ?string
    {
        return $this->resource;
    }

    public function setResource(string $resource): static
    {
        $this->resource = $resource;

        return $this;
    }

    public function getStartPointLatitude(): ?string
    {
        return $this->startPointLatitude;
    }

    public function setStartPointLatitude(string $startPointLatitude): static
    {
        $this->startPointLatitude = $startPointLatitude;

        return $this;
    }

    public function getStartPointLongitude(): ?string
    {
        return $this->startPointLongitude;
    }

    public function setStartPointLongitude(string $startPointLongitude): static
    {
        $this->startPointLongitude = $startPointLongitude;

        return $this;
    }

    public function getEndPointLatitude(): ?string
    {
        return $this->endPointLatitude;
    }

    public function setEndPointLatitude(string $endPointLatitude): static
    {
        $this->endPointLatitude = $endPointLatitude;

        return $this;
    }

    public function getEndPointLongitude(): ?string
    {
        return $this->endPointLongitude;
    }

    public function setEndPointLongitude(string $endPointLongitude): static
    {
        $this->endPointLongitude = $endPointLongitude;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): static
    {
        $this->duration = $duration;

        return $this;
    }

    public function getDistance(): ?string
    {
        return $this->distance;
    }

    public function setDistance(string $distance): static
    {
        $this->distance = $distance;

        return $this;
    }

    public function getDeparture(): ?\DateTimeInterface
    {
        return $this->departure;
    }

    public function setDeparture(\DateTimeInterface $departure): static
    {
        $this->departure = $departure;

        return $this;
    }

    public function getArrival(): ?\DateTimeInterface
    {
        return $this->arrival;
    }

    public function setArrival(\DateTimeInterface $arrival): static
    {
        $this->arrival = $arrival;

        return $this;
    }

    public function getDepartureHour(): ?\DateTimeInterface
    {
        return $this->departure_hour;
    }

    public function setDepartureHour(\DateTimeInterface $departure_hour): static
    {
        $this->departure_hour = $departure_hour;

        return $this;
    }

    public function getPrice(): ?Price
    {
        return $this->price;
    }

    public function setPrice(?Price $price): static
    {
        $this->price = $price;

        return $this;
    }

}
