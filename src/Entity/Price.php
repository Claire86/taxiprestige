<?php

namespace App\Entity;

use App\Entity\Itinerary;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\PriceRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: PriceRepository::class)]
class Price
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $startHour = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $endHour = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $startDay = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 5, scale: 2)]
    private ?string $amount = null;

    #[ORM\OneToMany(mappedBy: 'price', targetEntity: Itinerary::class)]
    private Collection $itinerarys;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $endDay = null;

    #[ORM\Column(length: 5, nullable: true)]
    private ?string $day = null;

    public function __construct()
    {
        $this->itinerarys = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartHour(): ?\DateTimeInterface
    {
        return $this->startHour;
    }

    public function setStartHour(\DateTimeInterface $startHour): static
    {
        $this->startHour = $startHour;

        return $this;
    }

    public function getEndHour(): ?\DateTimeInterface
    {
        return $this->endHour;
    }

    public function setEndHour(\DateTimeInterface $endHour): static
    {
        $this->endHour = $endHour;

        return $this;
    }

    public function getStartDay(): ?string
    {
        return $this->startDay;
    }

    public function setStartDay(string $startDay): static
    {
        $this->startDay = $startDay;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): static
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return Collection<int, Itinerary>
     */
    public function getItinerarys(): Collection
    {
        return $this->itinerarys;
    }

    public function addItinerary(Itinerary $itinerary): static
    {
        if (!$this->itinerarys->contains($itinerary)) {
            $this->itinerarys->add($itinerary);
            $itinerary->setPrice($this);
        }

        return $this;
    }

    public function removeItinerary(Itinerary $itinerary): static
    {
        if ($this->itinerarys->removeElement($itinerary)) {
            // set the owning side to null (unless already changed)
            if ($itinerary->getPrice() === $this) {
                $itinerary->setPrice(null);
            }
        }

        return $this;
    }

    public function getEndDay(): ?string
    {
        return $this->endDay;
    }

    public function setEndDay(?string $endDay): static
    {
        $this->endDay = $endDay;

        return $this;
    }

    public function getDay(): ?string
    {
        return $this->day;
    }

    public function setDay(?string $day): static
    {
        $this->day = $day;

        return $this;
    }

  
}
