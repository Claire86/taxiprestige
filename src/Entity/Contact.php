<?php

namespace App\Entity;

use App\Repository\ContactRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ContactRepository::class)]
class Contact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50, nullable: true)]
    #[Assert\Length(
        min: 2,
        max: 50,
        minMessage: 'Votre prénom doit avoir au moins {{ limit }} caractères',
        maxMessage: 'Votre prenom doit avoir au maximum {{ limit }} caractères',
    )]
    #[Assert\Regex(
        pattern: '/\d/',
        match: false,
        message: 'Votre prénom ne doit pas contenir de chiffres',
    )]
    protected ?string $firstName = null;

    #[ORM\Column(length: 50)]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Length(
        min: 2,
        max: 50,
        minMessage: 'Votre nom doit avoir au moins {{ limit }} caractères',
        maxMessage: 'Votre nom doit avoir au maximum {{ limit }} caractères',
    )]
    #[Assert\Regex(
        pattern: '/\d/',
        match: false,
        message: 'Votre nom ne peut pas contenir de chiffres',
    )]
    protected ?string $lastName = null;

    #[ORM\Column(length: 150)]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Email(
        message: 'L\'email{{ value }} n\'est pas un format valide.',
    )]
    #[Assert\Length(
        min: 2,
        max: 180,
        minMessage: 'Votre email doit avoir au moins {{ limit }} caractères',
        maxMessage: 'Votre email doit avoir au maximum {{ limit }} caractères',
    )]
    protected ?string $email = null;

    #[ORM\Column(length: 15, nullable: true)]
    #[Assert\Length(
        min: 10,
        max:15,
        minMessage: 'Votre numéro de téléphone doit avoir au moins {{ limit }} caractères',
        maxMessage: 'Votre numéro de téléphone doit avoir {{ limit }} caractères maximum',
    )]
    private ?string $phone = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Length(
        min: 10,
        minMessage: 'Votre message doit avoir au moins {{ limit }} caractères',
    )]
    private ?string $message = null;

    #[ORM\Column(length: 150)]
    #[Assert\NotBlank]
    #[Assert\NotNull()]
    #[Assert\Length(
        min: 2,
        max:100,
        minMessage: 'Le sujet doit avoir au moins {{ limit }} caractères',
        maxMessage: 'Le sujet doit avoir {{limit }} caractères maximum',
    )]
    private ?string $sujet = null;

    #[ORM\Column]
    #[Assert\NotNull()]
    private ?\DateTimeImmutable $createdAt = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): static
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): static
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;

        return $this;
    }

    public function getSujet(): ?string
    {
        return $this->sujet;
    }

    public function setSujet(string $sujet): static
    {
        $this->sujet = $sujet;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName(),
            'mail' => $this->getEmail(),
            'phone' => $this->getPhone(),
            'message' => $this->getMessage(),
            'subject' => $this->getSujet()
        ];
    }
}
