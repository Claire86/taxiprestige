<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230706204749 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'nullable true for duration, distance,arrival';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE route CHANGE duration duration NUMERIC(5, 2) DEFAULT NULL, CHANGE distance distance NUMERIC(5, 2) DEFAULT NULL, CHANGE arrival arrival DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE route CHANGE duration duration NUMERIC(5, 2) NOT NULL, CHANGE distance distance NUMERIC(5, 2) NOT NULL, CHANGE arrival arrival DATE NOT NULL');
    }
}
