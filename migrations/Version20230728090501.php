<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230728090501 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'update day end and start';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE price ADD start_day VARCHAR(20) DEFAULT NULL, ADD end_day VARCHAR(20) DEFAULT NULL, DROP day');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE price ADD day DATE NOT NULL, DROP start_day, DROP end_day');
    }
}
