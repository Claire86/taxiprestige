<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230707141854 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'changement du nom de la table Route en Itinerary';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE34ECB4E6');
        $this->addSql('CREATE TABLE itinerary (id INT AUTO_INCREMENT NOT NULL, price_id INT DEFAULT NULL, resource VARCHAR(255) NOT NULL, start_point_latitude NUMERIC(11, 8) NOT NULL, start_point_longitude NUMERIC(11, 8) NOT NULL, end_point_latitude NUMERIC(11, 8) NOT NULL, end_point_longitude NUMERIC(11, 8) NOT NULL, duration NUMERIC(5, 2) DEFAULT NULL, distance NUMERIC(5, 2) DEFAULT NULL, departure DATE NOT NULL, arrival DATE DEFAULT NULL, departure_hour TIME NOT NULL, INDEX IDX_FF2238F6D614C7E7 (price_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE itinerary ADD CONSTRAINT FK_FF2238F6D614C7E7 FOREIGN KEY (price_id) REFERENCES price (id)');
        $this->addSql('ALTER TABLE route DROP FOREIGN KEY FK_2C42079D614C7E7');
        $this->addSql('DROP TABLE route');
        $this->addSql('DROP INDEX UNIQ_E00CEDDE34ECB4E6 ON booking');
        $this->addSql('ALTER TABLE booking CHANGE route_id itinerary_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE15F737B2 FOREIGN KEY (itinerary_id) REFERENCES itinerary (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E00CEDDE15F737B2 ON booking (itinerary_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE15F737B2');
        $this->addSql('CREATE TABLE route (id INT AUTO_INCREMENT NOT NULL, price_id INT DEFAULT NULL, resource VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, start_point_latitude NUMERIC(11, 8) NOT NULL, start_point_longitude NUMERIC(11, 8) NOT NULL, end_point_latitude NUMERIC(11, 8) NOT NULL, end_point_longitude NUMERIC(11, 8) NOT NULL, duration NUMERIC(5, 2) DEFAULT NULL, distance NUMERIC(5, 2) DEFAULT NULL, departure DATE NOT NULL, arrival DATE DEFAULT NULL, departure_hour TIME NOT NULL, INDEX IDX_2C42079D614C7E7 (price_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE route ADD CONSTRAINT FK_2C42079D614C7E7 FOREIGN KEY (price_id) REFERENCES price (id)');
        $this->addSql('ALTER TABLE itinerary DROP FOREIGN KEY FK_FF2238F6D614C7E7');
        $this->addSql('DROP TABLE itinerary');
        $this->addSql('DROP INDEX UNIQ_E00CEDDE15F737B2 ON booking');
        $this->addSql('ALTER TABLE booking CHANGE itinerary_id route_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE34ECB4E6 FOREIGN KEY (route_id) REFERENCES route (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E00CEDDE34ECB4E6 ON booking (route_id)');
    }
}
